<?php

require('animal.php');
require('ape.php');
require('frog.php');
$sheep = new Animal("shaun");
$ape = new Ape("kera sakti");
$frog = new Frog("buduk");

echo "Nama hewan : ".$sheep -> nama."<br>"; // "shaun"
echo "Jumlah kaki : ".$sheep -> legs."<br>"; // 4
echo "Berdarah Dingin : ".$sheep -> cold_blooded; // "no"
echo "<br><br>";
echo "Nama hewan : ".$frog -> nama."<br>";
echo "Jumlah kaki : ".$frog -> legs."<br>";
echo "Berdarah Dingin : ".$frog -> cold_blooded."<br>";
echo "Jump : ".$frog -> jump;
echo "<br><br>";
echo "Nama hewan : ".$ape -> nama."<br>";
echo "Jumlah kaki : ".$ape -> legs."<br>";
echo "Berdarah Dingin : ".$ape -> cold_blooded."<br>";
echo "Jump : ".$ape -> yell."<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>
